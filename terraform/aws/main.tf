resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCqG9H5LAUpZlBDl2Bfda7BNerRValkUBC+VLjFBx6bSe+Ee2JwsBKVGMM0r8NqoENQT0d5nMd3UI71Xcr94SAUTkNONOs/uzPZLrnAk5YcsbGiNuZ3hLB6GSbEQZTKRkFmoo4f8w1hx5ep+GO0LBxdP3dK29NkLZx77ewLAkOjIyQaRazxXEOrnmJZiGl6Z8xKKhnDszMrskcbwebzqOwuuuSxNM7K5qkWuQt+MErt/T6IReRKIMnTDvk4bc8D7mgtYI9pAPPJ8t2GwkNXeZV8932buENel9jtVdXBRaQzhEbXmpm2ZYmE9h0bVCTMxxe+/dpbeA4qFRuGGkTg7hOzpWDePfTxUe7X4gZAeq5M4DW6SX5/rteEEG06d9UvkMJqDZEjhD+t9CNxxqK3UJNKWwbLLEu71jixVTJ5M8nZ9AqrTV+T0UF0kBXeNJQehz4tewaUIXWiK6+TNg8J2lT5C9yP12tMNGDqESTcdzMgs8gOlmlq/LMcJ4QfVAhYGwc= aurelien@aurelien-GE70-2OC-2OE"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 5000
    to_port   = 5000
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


provider "aws" {
  region = "eu-west-1"
}


resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = "t3.micro"
  security_groups = [aws_default_security_group.default.name]
  key_name = aws_key_pair.deployer.key_name

  tags = {
    Name = "HelloWorld"
  }
}

