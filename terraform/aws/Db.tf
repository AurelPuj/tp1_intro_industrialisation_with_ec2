resource "aws_dynamodb_table" "ma_super_db" {
  name           = "User_info"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "User_Id"

  attribute{

    name="User_Id"
    type= "S"

  }
}
